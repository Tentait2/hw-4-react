import {useEffect} from "react";
import {useDispatch} from "react-redux";


import ProductList from "../components/productList/ProductList";
import Modal from "../components/modal/Modal";
import {loadModals} from "../store/modals/modals-actions";
import {loadProducts} from "../store/products/products-actions";

import '../App.css'
function WishList(props) {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadProducts())
        dispatch(loadModals())
    }, [])

    return (
        <>
            <ProductList page={'wishlist'}/>
            <Modal/>
        </>
    );
}

export default WishList;
