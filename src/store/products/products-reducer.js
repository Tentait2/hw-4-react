import {ADD_PRODUCTS, SET_LOADING, SET_ERROR, TOGGLE_FAVORITE, TOOGLE_BUCKET} from './products-actions'

const initialState = {
    status: 'idle',
    list: [],
    error: null,
    favorites: [],
    bucketItems: []
}
export const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PRODUCTS :
            return {
                ...state,
                list: action.payload,
                status: 'fullfield'
            }
        case TOGGLE_FAVORITE :
            let favouriteCardID = action.payload;
            const favorites = [...state.favorites];
            if (favorites.includes(favouriteCardID)) {
                return {
                    ...state,
                    favorites: favorites.filter((id) => id !== favouriteCardID),
                };
            } else {
                return {
                    ...state,
                    favorites: [...favorites, favouriteCardID],
                };
            }
        case TOOGLE_BUCKET :
            let bucketCardID = action.payload;
            const bucketItems = [...state.bucketItems];
            if (bucketItems.includes(bucketCardID)) {
                return {
                    ...state,
                    bucketItems: bucketItems.filter((id) => id !== bucketCardID),
                };
            } else {
                return {
                    ...state,
                    bucketItems: [...bucketItems, bucketCardID],
                };
            }
        case SET_LOADING :
            return {
                ...state,
                status: 'loading',
                error: null
            }
        case SET_ERROR :
            return {
                ...state,
                status: 'rejected',
                error: action.payload
            }
        default :
            return state
    }
}