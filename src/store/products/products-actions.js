import {client} from "../../api";

export const ADD_PRODUCTS = '@@products/ADD_PRODUCTS'
export const SET_LOADING = "@@products/SET_LOADING"
export const SET_ERROR = "@@products/SET_ERROR"
export const TOGGLE_FAVORITE  = '@@products/TOGGLE_FAVORITE'
export const TOOGLE_BUCKET = '@@products/TOOGLE_BUCKET'
const addProducts = (products) => ({
    type : ADD_PRODUCTS,
    payload: products
})
export const toggleFavorite = (cardId) => {
    return {
        type: TOGGLE_FAVORITE,
        payload: cardId,
    };
};

export const toogleBucket = (cardId) => ({
    type: TOOGLE_BUCKET,
    payload : cardId
})
export const setLoading = () => ({
    type: SET_LOADING
})
export const setError = (error) => ({
    type: SET_ERROR,
    payload: error
})

export const loadProducts = () => (dispatch) =>{
    dispatch(setLoading())
    client.get('productCollection.json')
        .then(data => dispatch(addProducts(data)))
        .catch(error => dispatch(setError(error)))
}