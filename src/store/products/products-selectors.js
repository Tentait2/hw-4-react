export const selectAllItems = state => state.products.list

export const selectFavoriteItems = state => state.products.list.filter(item => state.products.favorites.includes(item.id))
export const selectBucketItems = state => state.products.list.filter(item => state.products.bucketItems.includes(item.id))