export const selectActiveModal = state => {
    const modalId = state.modals.modalId;
    const modalsInfo = state.modals.modalsInfo || [];

    const modal = modalsInfo.find(item => item.modalId === modalId) || {};

    const headerText = modal.headerText ?? '';
    const modalText = modal.modalText ?? '';
    const backgroundColor = modal.backgroundColor ?? '';
    const closeButton = modal.closeButton ?? false;

    return {
        headerText,
        modalText,
        backgroundColor,
        closeButton,
        modalId
    };
}