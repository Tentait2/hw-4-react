import {client} from "../../api";

export const ADD_MODALS = '@@modals/ADD_MODALS'
export const OPEN_MODAL = '@@modals/OPEN_MODAL'
export const CLOSE_MODAL = '@@modals/CLOSE_MODAL'

const addModals = (modals) => ({
    type : ADD_MODALS,
    payload: modals
})
export const openModal = (modalId, activeCardId) => ({
    type: OPEN_MODAL,
    payload: {
        modalId, activeCardId
    }
})
export const closeModal = () => ({
    type: CLOSE_MODAL
})


export const loadModals = () => (dispatch) =>{
    client.get('modalWindowDeclarations.json')
        .then(data => dispatch(addModals(data)))
}