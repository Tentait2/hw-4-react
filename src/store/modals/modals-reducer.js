import {ADD_MODALS, CLOSE_MODAL, OPEN_MODAL} from "./modals-actions";

const initialState = {
    openModal : false,
    modalsInfo : [],
    error : null,
    modalId : ""
}
export const modalsReducer = (state = initialState, action) => {
    switch (action.type){
        case ADD_MODALS : return {
            ...state,
            modalsInfo: action.payload,
        }
        case OPEN_MODAL : return {
            ...state,
            openModal: true,
            modalId : action.payload.modalId,
            activeCardId: action.payload.activeCardId
        }
        case CLOSE_MODAL : return {
            ...state,
            openModal : false
        }

        default : return state
    }
}