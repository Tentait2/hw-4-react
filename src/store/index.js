import {applyMiddleware, createStore} from "redux";
import {rootReducer} from './root-reducer'
import {persistStore, persistReducer} from "redux-persist";
import storage from 'redux-persist/lib/storage'
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

const persistConfig = {
  key: 'root',
  storage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(
    persistedReducer, composeWithDevTools(applyMiddleware(thunk))
)

export {store}
export const persistor = persistStore(store)