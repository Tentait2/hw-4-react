import {combineReducers} from 'redux';
import {productsReducer} from "./products/products-reducer";
import {modalsReducer} from "./modals/modals-reducer";
export const rootReducer = combineReducers({products : productsReducer, modals : modalsReducer});