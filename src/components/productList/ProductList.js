
import './ProductList.scss'
import {useSelector} from "react-redux";
import ProductCard from "../productCard/ProductCard";
import LoadingSpinner from "../loadingSpinner/LoadingSpinner";
import {selectAllItems, selectBucketItems, selectFavoriteItems} from '../../store/products/products-selectors'

const selectors = {
    'home' : selectAllItems,
    'bucket' : selectBucketItems,
    'wishlist' : selectFavoriteItems
}
const ProductList = (props) => {
    const {error, status} = useSelector(state => state.products)

    const selector = selectors[props.page]
    const list = useSelector(selector);

    return (
        <ul className="card-list">
            {status === 'loading' ? <LoadingSpinner/> : list.map((product, index) => (
                <ProductCard key={index} data={product} />
            ))}
        </ul>

    )

}




export default ProductList