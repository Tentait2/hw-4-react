import PropTypes from "prop-types";
import './ProductCard.scss'
import {useDispatch, useSelector} from "react-redux";
import {closeModal, openModal} from "../../store/modals/modals-actions";
import {toggleFavorite} from '../../store/products/products-actions'

const ProductCard = (props) => {
    const dispatch = useDispatch()
    const favoritesCard = useSelector(state => state.products.favorites)
    const bucketCard = useSelector(state => state.products.bucketItems)
    const isCardStarred = (cardId) => {
        return favoritesCard.includes(cardId);
    };
    const isCardInBucket = (cardId) => {
        return bucketCard.includes(cardId);
    };

    const {name, price, vendorCode, color, imgUrl, id} = props.data
    return (
        <li className={'card-list-item'}>
            <img className={'list-img'} src={imgUrl} alt="item-img"/>
            <div className={'list-item-container'}>
                <p className={'list-item-text'}>Name : {name}</p>
                <p className={'list-item-text'}> Price :{price} UAH</p>
                <p className={'list-item-text'}> Vendor Code : {vendorCode}</p>
                <p className={'list-item-text'}> Color : {color}</p>
                <p className={'item-favourite'} onClick={() => dispatch(toggleFavorite(id))}>
                    {isCardStarred(id) ? '★' : '☆'}
                </p>
                <button id={id} className={'button-primary'} onClick={!isCardInBucket(id) ? () => {
                    dispatch(openModal('modalID1', id))
                } : () => {
                    dispatch(openModal('modalID2', id))
                }}>{isCardInBucket(id) ? 'Remove from bucket' : 'Add to bucket'}</button>
            </div>
        </li>

    )
}

ProductCard.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    imgUrl: PropTypes.string
}


export default ProductCard
