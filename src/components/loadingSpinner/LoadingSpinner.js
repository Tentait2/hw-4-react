import loaderGif from '../../resourses/img/Loading_icon.gif'

const LoadingSpinner = () => {
    return (
        <>
            <img src={loaderGif} alt="loaderGif"/>
            <p>Wait a few seconds</p>
        </>
    )
}

export default LoadingSpinner