import img from '../../resourses/img/cross.svg'
import {useDispatch, useSelector} from "react-redux";
import {closeModal} from "../../store/modals/modals-actions";
import {selectActiveModal} from "../../store/modals/modals-selectors";
import {toogleBucket} from "../../store/products/products-actions";
import './Modal.scss'

const Modal = (props) => {
    const dispatch = useDispatch()
    const {openModal, activeCardId} = useSelector(state => state.modals)
    const {headerText, modalText, backgroundColor, closeButton, modalId} = useSelector(selectActiveModal);


    const handleCloseModal = (e) => {
        if (e.target.className === 'background' || e.target.className === 'close-modal' || e.target.value === 'closeModal') {
            dispatch(closeModal())
        }
    }


    if (openModal){
        return (
            <div key={modalId} className={'background'} onClick={handleCloseModal}>
                <div className={'modal'} style={{backgroundColor}}>
                    <h1>{headerText}</h1>
                    <p>{modalText}</p>
                    {closeButton ? <img className={'close-modal'} src={img} alt="#"/> : null}
                    <button onClick={() => {
                        dispatch(toogleBucket(activeCardId))
                        dispatch(closeModal())
                    }} className={'button-modal'} key={1}>Yes</button>
                    <button className={'button-modal'} value={'closeModal'} key={2}>No</button>
                </div>
            </div>
        )
    }

}


export default Modal