import {Link, NavLink} from "react-router-dom";

import "./AppHeader.scss"
import logo from '../../resourses/img/Jumpman_logo.svg.png'
import cartIcon from '../../resourses/img/cart-icon.png'
import favouriteImg from '../../resourses/img/favourite-icon.png'
import {useSelector} from "react-redux";



const AppHeader = () => {
    const favoritesLength = useSelector(state => state.products.favorites.length)
    const bucketItemsLength = useSelector(state => state.products.bucketItems.length)
        return (
            <header className={'page-header'}>
                <Link to={'/'}>
                    <img className={'page-logo'} src={logo} alt={'logo'}></img>
                </Link>

                <h1 className={'page-title'}>Jordan Shop</h1>
                <div className={'header-icons-wrapper'}>
                    <NavLink style={({isActive}) => ({color: isActive ? 'red' : 'inherit'})}
                             to='/bucket'> <img className={'header-icons'} src={cartIcon} alt="cartIcon"/></NavLink>
                    <p className={'cart-length'}>{bucketItemsLength}</p>

                    <NavLink style={({isActive}) => ({color: isActive ? 'red' : 'inherit'})}
                             to='/wishlist'> <img className={'header-icons'} src={favouriteImg} alt="cartIcon"/></NavLink>
                    <p className={'favourite-length'}>{favoritesLength}</p>
                </div>
            </header>
        )
    }



export default AppHeader