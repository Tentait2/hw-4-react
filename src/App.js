import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {loadProducts} from "./store/products/products-actions";

import './App.css';

import AppHeader from "./components/appHeader/AppHeader";
import {loadModals} from "./store/modals/modals-actions";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./pages/home";
import WishList from "./pages/wishList";
import Bucket from "./pages/bucket";

function App() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadProducts())
        dispatch(loadModals())
    }, [])


    return (
        <div className="App">
            <BrowserRouter>
                <header>
                    <AppHeader/>
                </header>
                <Routes>
                    <Route path={'/'} element={<Home/>}/>
                    <Route path={'/bucket'} element={<Bucket/>}/>
                    <Route path={'/wishlist'} element={<WishList/>}/>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
