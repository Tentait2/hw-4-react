import React from 'react';
import {Provider} from 'react-redux';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';
import { store } from './store';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import {PersistGate} from "redux-persist/integration/react";
import {persistor} from "./store";


const rootElement = document.getElementById('root');
const root = createRoot(rootElement);
root.render(
    <React.StrictMode>
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor} >
            <App/>
        </PersistGate>
    </Provider>
</React.StrictMode>,);

